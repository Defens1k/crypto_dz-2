#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <functional>
/*
#include <cryptopp/modes.h>
#include <cryptopp/osrng.h>
#include <cryptopp/rsa.h>
#include <cryptopp/sha.h>
#include <cryptopp/files.h>
*/
#include "rsa_functions.h"


int main () {

    CryptoPP::AutoSeededRandomPool rng;

    CryptoPP::RSA::PrivateKey privateKey;
 //   CryptoPP::OFB_Mode<CryptoPP::AES>::Encryption s_localRNG;

    privateKey.GenerateRandomWithKeySize(rng, 2048);

    CryptoPP::RSA::PublicKey publicKey(privateKey);
    SavePrivateKey("keys/id_rsa", privateKey);
    SavePublicKey("keys/id_rsa.pub", publicKey);
}
