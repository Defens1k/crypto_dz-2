#include "Car.h"

Car::Car(CryptoPP::RSA::PublicKey input_key) {
    pendant_pubkey = input_key;
    std::srand(time(NULL));
}

std::string Car::handshake() const {
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::string chellange = generate_chellange();
    actual_chellanges.insert(chellange);
    std::cout << "car->pendant send data: " << chellange << std::endl;
    return chellange;
}

bool Car::response(std::string chellange, std::string eds) const {
    std::cout << "pendant->car send data: " << chellange << eds << std::endl;
    if (actual_chellanges.find(chellange) == actual_chellanges.end()) {
        return false; // Проверяем, посылали ли мы такой chellange
    }
    actual_chellanges.erase(chellange);
    if (decrypt(eds, pendant_pubkey) != num_to_str(std::hash<std::string>{}(chellange))) {
        std::cerr << decrypt(eds, pendant_pubkey) << std::endl;
        return false;  // проверка ЭЦП
    }
    std::cout << "Car is open!";
    return true;
}

std::string Car::generate_chellange() const {
    std::string chellange;
    for (size_t i = 0; i < 10; i++) {
        chellange = chellange + num_to_str( std::rand());
    }
    return chellange;
}
