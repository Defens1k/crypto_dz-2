#include "Pendant.h"

Pendant::Pendant(CryptoPP::RSA::PrivateKey && input_key) :  
                                                privkey(input_key),
                                                pubkey(input_key) {;}

bool Pendant::open_car(const Car & car) {
    std::cout << "pendant->car send handshake wichout data" << std::endl;
    std::string chellange = car.handshake();
    return car.response(chellange, encrypt(num_to_str(std::hash<std::string>{}(chellange)), privkey)); // Шифруем хэш от chellange, подписывая его
}
