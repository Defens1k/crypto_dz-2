#include <iostream>
#include <string>

#include "rsa_functions.h"
#include "Car.h"
#include "Pendant.h"

int main () {

    CryptoPP::AutoSeededRandomPool rng;  

    CryptoPP::RSA::PrivateKey privateKey;

    LoadPrivateKey("keys/id_rsa", privateKey);
    CryptoPP::RSA::PublicKey publicKey(privateKey);
    Car car(publicKey);
    Pendant pendant(std::move(privateKey));
    pendant.open_car(car);
}
