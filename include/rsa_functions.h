#ifndef RSA_FUNCTIONS_H_
#define RSA_FUNCTIONS_H_

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <functional>

#include <modes.h>
#include <osrng.h>
#include <rsa.h>
#include <pssr.h>
#include <sha.h>
#include <files.h>

/*   Файл вспомогательных функций для работы с ключами CryptoPP::RSA */

CryptoPP::SecByteBlock str_to_block(const std::string & s) ;

std::string block_to_str(const CryptoPP::SecByteBlock & block) ;


void Save(const std::string& filename, const CryptoPP::BufferedTransformation& bt) ;

void SavePublicKey(const std::string& filename, const CryptoPP::RSA::PublicKey& key) ;

void SavePrivateKey(const std::string& filename, const CryptoPP::RSA::PrivateKey& key) ;

void Load(const std::string& filename, CryptoPP::BufferedTransformation& bt) ;

void LoadPrivateKey(const std::string& filename, CryptoPP::RSA::PrivateKey& key) ;

void LoadPublicKey(const std::string& filename, CryptoPP::RSA::PublicKey& key) ;

template <typename T>
std::string num_to_str(T num) {
    std::stringstream s;
    s << num;
    std::string str;
    s >> str;
    return str;
}

std::string encrypt(std::string s, const CryptoPP::RSA::PrivateKey & key); 

std::string decrypt(std::string s, const CryptoPP::RSA::PublicKey & key); 
#endif //RSA_FUNCTIONS_H_
