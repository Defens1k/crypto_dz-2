#ifndef PENDANT_H_
#define PENDANT_H_

#include "rsa_functions.h"
#include "Car.h"
class Pendant {
 public:
    Pendant(CryptoPP::RSA::PrivateKey &&);
    bool open_car(const Car&);
 private:
    CryptoPP::RSA::PrivateKey privkey;
    CryptoPP::RSA::PublicKey pubkey;
};


#endif //PENDANT_H_
