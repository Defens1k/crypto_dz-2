#ifndef CAR_H_
#define CAR_H_
#include "rsa_functions.h"
#include <set>
#include <sstream>
#include <time.h>
#include <thread>
#include <chrono>

class Car {
 public:
    Car(CryptoPP::RSA::PublicKey);  //car must have pendant public key
    std::string handshake() const;   // return unique handshake id as a chellange
    bool response(std::string, std::string) const ;
 private:
    CryptoPP::RSA::PublicKey pendant_pubkey;
    mutable std::set<std::string> actual_chellanges; //
    std::string generate_chellange() const;
};


#endif //CAR_H_0 
